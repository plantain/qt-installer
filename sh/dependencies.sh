#!/bin/bash

# Установка зависимостей
apt-get install -y \
gnupg2 \
git curl zsh \
build-essential \
cmake meson \
qmlscene \
qt5-default \
qtbase5-examples \
qtdeclarative5-examples \
mesa-common-dev \
libglu1-mesa-dev \
libfontconfig1 \
libqt5webkit5-dev
