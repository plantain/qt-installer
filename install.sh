#!/bin/bash
#
# install.sh
#
# Copyright 2019 Alex Grigorev <prog.haskell@gmail.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
# MA 02110-1301, USA.

url=http://download.qt.io/official_releases/online_installers/qt-unified-linux-x64-online.run
installer=qt-unified-linux-x64-online.run
log=install.log
sudo="exec sudo"
exec="exec sudo --user=#1000 --group=#1000"

$exec chmod +x *.sh sh/*.sh & wait

echo -n "Установить Qt Creator? (Y/n) "

read one
case "$one" in
    ""|y|Y)
        echo "1) Из репозитория       |"
        echo "2) Запустить установщик | -> (Выбор по умолчанию)"
        echo "3) Прервать установку   |"
        echo -n "Продолжить? [1/2/3] "
        read two
        case "$two" in
            1)
                $sudo echo "Установка зависимостей..." & wait
                $sudo ./sh/dependencies.sh > $log & wait
                $sudo echo "Установка среды разработки..." & wait
                $sudo ./sh/qtcreator.sh >> $log & wait
                ;;
            ""|2)
                if [[ -f $installer ]]; then
                    echo "Запуск установщика..."
                    $exec ./$installer >> $log & wait
                else
                    $sudo echo "Установка зависимостей..." & wait
                    $sudo ./sh/dependencies.sh > $log & wait
                    echo "Загрузка установщика..."
                    $exec wget $url & wait
                    $exec chmod +x $installer & wait
                    echo "Запуск установщика..."
                    $exec ./$installer >> $log & wait
                fi
                ;;
            [а-я]|[А-Я]|[a-z]|[A-Z]|[0-9])
                echo "Прервано."
                exit 0
        esac
        ;;
    n|N)
        ;;
    [а-я]|[А-Я]|[a-z]|[A-Z]|[0-9])
        echo "Прервано."
        exit 0
esac

echo -n "Установить шрифты? (Y/n) "

read three
case "$three" in
    ""|y|Y)
        echo "1) Google Noto fonts                           |"
        echo "2) Google Roboto fonts                         |"
        echo "3) Mozilla Fira fonts                          |"
        echo "4) Fira Code (моноширинный шрифт с лигатурами) |"
        echo "5) Установить все выше перечисленное           | -> (Выбор по умолчанию)"
        echo "6) Прервать установку                          |"
        echo -n "Продолжить? [1/2/3/4/5/6] "
        read four
        case "$four" in
            1)
                $sudo echo "Установка Google Noto fonts..." & wait
                $sudo apt-get install -y fonts-noto-* >> $log & wait
                ;;
            2)
                $sudo echo "Установка Google Roboto fonts..." & wait
                $sudo apt-get install -y fonts-roboto-* >> $log & wait
                ;;
            3)
                echo "Установка Mozilla Fira fonts..."
                if [[ ! -d "Fira" ]]; then
                    $exec git clone https://github.com/mozilla/Fira.git & wait
                    $sudo mkdir -p /usr/share/fonts/otf/Fira & wait
                    $sudo cp Fira/otf/* /usr/share/fonts/otf/Fira/ & wait
                    $sudo fc-cache -f -v > /dev/null & wait
                else
                    $sudo mkdir -p /usr/share/fonts/otf/Fira & wait
                    $sudo cp Fira/otf/* /usr/share/fonts/otf/Fira/ & wait
                    $sudo fc-cache -f -v > /dev/null & wait
                fi
                ;;
            4)
                echo "Установка Fira Code..."
                $sudo apt-get install fonts-firacode >> $log & wait
                ;;
            ""|5)
                $sudo echo "Установка шрифтов..." & wait
                $sudo ./sh/fonts.sh >> $log & wait
                if [[ ! -d "Fira" ]]; then
                    $exec git clone https://github.com/mozilla/Fira.git & wait
                    $sudo mkdir -p /usr/share/fonts/otf/Fira & wait
                    $sudo cp Fira/otf/* /usr/share/fonts/otf/Fira/ & wait
                    $sudo fc-cache -f -v > /dev/null & wait
                else
                    $sudo mkdir -p /usr/share/fonts/otf/Fira & wait
                    $sudo cp Fira/otf/* /usr/share/fonts/otf/Fira/ & wait
                    $sudo fc-cache -f -v > /dev/null & wait
                fi
                ;;
            [а-я]|[А-Я]|[a-z]|[A-Z]|[0-9])
                echo "Прервано."
                exit 0
        esac
        ;;
    n|N)
        ;;
    [а-я]|[А-Я]|[a-z]|[A-Z]|[0-9])
        echo "Прервано."
        exit 0
esac

echo -n "Установить oh-my-zsh? (Y/n) "

read five
case "$five" in
    ""|y|Y)
        echo "Установка oh-my-zsh..."
        $exec sh -c "$(curl -fsSL https://raw.github.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"
        ;;
    [а-я]|[А-Я]|[a-z]|[A-Z]|[0-9])
        echo "Прервано."
        exit 0
esac

$sudo chown -R 1000:1000 * & wait
